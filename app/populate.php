<?php declare(strict_types = 1);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');


require ROOT . 'vendor/autoload.php';

$loadEnvironment = Dotenv\Dotenv::createImmutable(__DIR__);
$loadEnvironment->safeLoad();

use Mailservice\Balanceserver\Database\Entity\NewsletterQueue;
use Mailservice\Balanceserver\Database\DataAccess\NewsletterData;

$data = new NewsletterData("acm_database3");

$date =  new DateTime;
$result = $date->format('Y/m/d H:i:s');
var_dump($result);

$data->insert([
    "id" => rand(),
    "to_email" => "grega@email.com",
    "subject" => "Subject",
    "body" => "Email body Email body Email body Email body",
    "is_html" => true,
    "datetime_insert" => $date->format('Y/m/d H:i:s'),
    "send_error" => false,
    "send_date" => null,




]);