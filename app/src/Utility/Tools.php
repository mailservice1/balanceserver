<?php

namespace Mailservice\Balanceserver\Utility;

interface ITools
{
    const SMTP_PATTERN = "/smtp\.*\.*/";
}

class Tools implements ITools
{
    public static function verifySmtpHost( string $_smtp_host ): bool
    {
        return preg_match( self::SMTP_PATTERN, $_smtp_host );
    }
}