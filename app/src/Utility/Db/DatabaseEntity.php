<?php

namespace Mailservice\Balanceserver\Utility\Db;

interface DatabaseEntity
{   
    /**
     * 
     * @access public
     * @method fetchAll
     * Populates entity list
     * 
     */
    public function fetchAll(): void;
    
}