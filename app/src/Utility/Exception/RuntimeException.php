<?php

namespace Mailservice\Balanceserver\Utility\Exception;

use Mailservice\Balanceserver\Core\Base\Event;


final class RuntimeException extends \Exception
{   
    private Event $event;

    private string $eventNote;

    public function __construct( Event $_event, ?string $_event_note = '' )
    {
        $this->event = $_event;

        $this->eventNote = $_event_note;
    }

    public function getEvent(): Event 
    {
        return $this->event;
    }
    public function getNote(): string 
    {
        return $this->eventNote;
    }
}
