<?php

namespace Mailservice\Balanceserver\Utility\Exception;


enum Error 
{
    case E1402;

    case E1404;

    case E2401;


    public function errorDescription(): string
    {
        return match($this)
        {
            Error::E1402 => 'Database settings',

            Error::E1404 => 'Environment smtp settings',

            Error::E2401 => 'Mising cluster settings'

        };
    }
}