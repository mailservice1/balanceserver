<?php

namespace Mailservice\Balanceserver\Utility\Exception;

interface IBuildException
{   
    /**
     * @method describe
     * @return string
     * Descibes error from app error settings.
     * 
     */
    public function describe(): string;

    const ERROR_TEXT = 'Build ERROR: ';
}

final class BuildException extends \Exception implements IBuildException
{   
    private Error $error;

    public function __construct( string $_error_detail, Error $_error )
    {
        $this->error = $_error;
    }
    public function describe(): string 
    {
        return self::ERROR_TEXT . $this->error->errorDescription();
    }
}
