<?php

namespace Mailservice\Balanceserver\Utility\ContractModel;


interface Signed
{
    public function hash(): void;
}