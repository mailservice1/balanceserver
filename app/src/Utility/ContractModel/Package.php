<?php

namespace Mailservice\Balanceserver\Utility\ContractModel;


interface Package
{
    public function package(): string;
}