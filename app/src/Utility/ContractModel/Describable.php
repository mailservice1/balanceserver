<?php

namespace Mailservice\Balanceserver\Utility\ContractModel;


interface Describable
{
    public function describe(): string;
}