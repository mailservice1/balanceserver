<?php

namespace Mailservice\Balanceserver\Utility;


abstract class Shell 
{   
    /**
     * @access protected
     * @method newGuid
     * @return string Guid
     */
    protected static function newGuid(): string 
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X'
        , mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479)
        , mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    protected static function saveSession( mixed $_data_to_store ): void
    {   
        session_start();
        $_SESSION['CYCLE_SESSION'] = $_data_to_store;
    }
    
    public static function incomeing()
    {
        if(file_get_contents('php://input'))
            {
                return json_decode(file_get_contents('php://input'));
            }
    }
    
    
}