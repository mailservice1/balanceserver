<?php

namespace Mailservice\Balanceserver\Utility;


interface IEncryption 
{
    public const CYFER = 'AES-128-CBC';

    public const HMACOPT = 'sha256';
}

abstract class Encryption implements IEncryption
{   
    private static string $key;

    public function encrypted( string $_key ): string
    {   
        self::$key = $_key;
        return self::sslEncrypt( json_encode( $this ) );
    }

    /**
     * @method encrypt
     * Open ssl enryption
     */
    protected function sslEncrypt( string $_content_to_encrypt): string
    {   
        return $this->runEncryptionSequence($_content_to_encrypt, self::$key, self::CYFER, self::HMACOPT );
    }
    private function runEncryptionSequence(string $_content_to_encrypt, string $key, string $cipher, string $hmac_opt): string
    {   
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($_content_to_encrypt, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac($hmac_opt, $ciphertext_raw, $key, $as_binary=true);
        return (string)$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    }

    /**
     * @method decrypt
     * Open ssl enryption
     */
    protected static function sslDecrypt( string $_content_to_decrypt, string $_key ): false | string
    {              
        return self::runDecryptSequnce($_content_to_decrypt, $_key , self::CYFER, self::HMACOPT);
    }
    private static function runDecryptSequnce( string $_content_to_decrypt, string $key, string $cipher, string $hmac_opt ): false | string
    {
        $c = base64_decode($_content_to_decrypt);
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac($hmac_opt, $ciphertext_raw, $key, $as_binary=true);
        if (hash_equals($hmac, $calcmac))
            return $original;
        return false;
    }
}