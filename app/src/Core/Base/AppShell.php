<?php

namespace Mailservice\Balanceserver\Core\Base;

use Mailservice\Balanceserver\Utility;
use Mailservice\Balanceserver\Utility\Exception\RuntimeException;
use Mailservice\Balanceserver\Core\Base\Event;
use Mailservice\Balanceserver\Utility\Tools;

use Mailservice\Balanceserver\Contracts;

use Mailservice\Balanceserver\Contracts\CycleSession;
use Mailservice\Balanceserver\Database\Entity\NewsletterQueue;

use Mailservice\Balanceserver\Contracts\Cluster\Letter;
use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SmtpSettings;

use Mailservice\Balanceserver\Core\ClusterController;
use Mailservice\Balanceserver\Core\ReportGenerator;

/**
 * @abstract 
 * Exteds:
 * @see final Application
 * @see Sequence
 * 
 */
abstract class AppShell extends Utility\Shell implements IAppShell
{
    abstract public function run(): void;

    public static Contracts\CycleSession $cycleSession;

    public static function report( Event $_event, string $_report_note ): void
    {   
        ReportGenerator::report( self::$cycletimestamp, $_event, $_report_note );
    }

    public static function reportFile(): string
    {
        return ReportGenerator::reportFile();
    }
    
    /**
     * @method emailPacage
     * @var NewsletterQueue
     * Maps all email entity object to contract letter object. 
     * Letter object is send to cluster server. 
     * @return Letter if not exception is reported in cycle report. 
     * 
     */
    public static function emailPacage( NewsletterQueue $waitingEmail ): Letter | null
    {   
        try
        {
            $emailId = $waitingEmail->id;
            $e = ' EmailId:' . $waitingEmail->id . ' | ';
            $recipient = $waitingEmail->to_email;
            if( !$recipient ) throw new RuntimeException( Event::BROKEN_EMAIL, $e . 'missing recipient' );

            if( !filter_var($recipient, FILTER_VALIDATE_EMAIL) ) throw new RuntimeException( Event::BROKEN_EMAIL, $e . 'incorrect form of recipient' );

            # Create cluster contract
            return new Letter(
                $emailId,
                $recipient,
                $waitingEmail->subject,
                $waitingEmail->body,
                $waitingEmail->is_html
            );
        }
        catch( RuntimeException $e )
        {
            self::report( $e->getEvent(), $e->getNote() );
            return null;
        }   
    }
    
    /**
     * @see ClusterController
     * @see Sequence
     */
    protected static ClusterController $clusterController;


    /**
     * @access protected
     * @method stratNewCylce
     * @return CycleSession
     * 
     */
    protected function startNewCycle(): Contracts\CycleSession
    {
        self::$cycletimestamp = time();
        self::$cycleId = parent::newGuid();

        $newCycleContract = new Contracts\Cycle( self::$cycleId, (int)self::$cycletimestamp );       
        $newCycleSession = new Contracts\CycleSession( $newCycleContract );

        /**
         * Construct new cluster controler. It send ping to all nodes
         * and creates active cluster server list.
         */
        self::$clusterController = new ClusterController;
        
        /**
         * Save session - // TODO: Control cycle by session - optional
         */
        parent::saveSession( $newCycleSession );

        ReportGenerator::report(
            self::$cycletimestamp,
            Event::NEW_CYCLE,
            $newCycleContract->describe() );
        
        unset( $newCycleContract );

        return $newCycleSession;

    }

    protected static int $cycletimestamp;

    private static string $cycleId;

    protected static SmtpSettings $smtpSettings;

    /**
     * 
     * @method loadEnvironment
     * Checks all environment. Kill exception!
     * //TODO: ssl key binidngs! 
     */
    public static function loadEnvironment(): void 
    {
        if( !$_ENV['DATABASE_TYPE'] ) throw new BuildException("Database type setting", ERROR::E1402);
        if( !$_ENV['DATABASE_HOST'] ) throw new BuildException("host setting", ERROR::E1402);
        if( !$_ENV['DATABASE'] ) throw new BuildException("database setting", ERROR::E1402);
        if( !$_ENV['DATABASE_USER'] ) throw new BuildException("user database setting", ERROR::E1402);
        if( !$_ENV['DATABASE_PASSWORD'] ) throw new BuildException("user password setting", ERROR::E1402);

        if( !$_ENV['SMTP_SERVER_HOST'] ) throw new BuildException("missing environment server host", ERROR::E1404);
        if( !Tools::verifySmtpHost($_ENV['SMTP_SERVER_HOST']) ) throw new BuildException("incorrect form of smtp server host", ERROR::E1404);

        if( !$_ENV['SMTP_SERVER_USERNAME'] ) throw new BuildException("missing environment smtp username", ERROR::E1404);
        if( !$_ENV['SMTP_SERVER_SENDER'] ) throw new BuildException("missing environment smtp sender", ERROR::E1404);

        if( !$_ENV['SMTP_SERVER_PORT'] ) throw new BuildException("missing environment smtp port", ERROR::E1404);

        self::$smtpSettings = new SmtpSettings(
            (string)$_ENV['SMTP_SERVER_HOST'],
            (int)$_ENV['SMTP_SERVER_PORT'],
            (int)$_ENV['SMTP_SERVER_USERNAME'],
            (int)$_ENV['SMTP_SERVER_PASSWORD'],
            (int)$_ENV['SMTP_SERVER_SENDER'],
            (int)$_ENV['SMTP_SERVER_SENDER'],
            (int)$_ENV['OPENSSL_CIPHER'],
            false
        );
    }


}
