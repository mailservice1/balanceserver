<?php

namespace Mailservice\Balanceserver\Core\Base;

use Mailservice\Balanceserver\Database\Entity\NewsletterQueue;
use Mailservice\Balanceserver\Core\Base\Event;
use Mailservice\Balanceserver\Contracts\Cluster\Letter;


interface IAppShell
{
    public function run(): void;

    public static function report( Event $_event, string $_report_note ): void;

    public static function emailPacage( NewsletterQueue $waitingEmail ): Letter | null;

    public const BR = "\n";

}