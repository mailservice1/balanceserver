<?php

namespace Mailservice\Balanceserver\Core\Base;


enum Event 
{
    case NEW_CYCLE;

    case SUB_CYCLE;

    case NO_TABLE;

    case NO_EMAIL_QUEUE;

    case EMAIL_QUEUE;

    case BROKEN_EMAIL;

    case SERVER_UNREACHABLE;

    case SERVER_CONNECTION;

    case PACKAGE_SEND;

    case RESPONDED;

    case RESPONSE_ERROR;

    case RESPONSE_SUCCESS;

    case RESPONSE_FAILED;

    case RESPONSE_COMPLETED;



    public function eventDescription(): string
    {
        return match($this)
        {
            Event::NEW_CYCLE => 'New cycle',

            Event::SUB_CYCLE => 'Sub cycle for database: ',

            Event::NO_TABLE => 'no table in database ',

            Event::NO_EMAIL_QUEUE => 'no emails  ',

            Event::EMAIL_QUEUE => ' email waiting to be send:  ',

            Event::PACKAGE_SEND => '>>> Pacage send to node: ',

            Event::BROKEN_EMAIL => ' email data are broken  ',

            Event::SERVER_CONNECTION => 'curl settings error ',

            Event::SERVER_UNREACHABLE => 'cluster server is no reachable ',

            Event::RESPONDED => 'RESPONDED >>> subcycle pacage id:',

            Event::RESPONSE_ERROR => ' wrong response ',

            Event::RESPONSE_SUCCESS => ' email send',

            Event::RESPONSE_FAILED => 'ERROR sending ',

            Event::RESPONSE_COMPLETED => '..............'

        };
    }
}