<?php

namespace Mailservice\Balanceserver\Core;

use Mailservice\Balanceserver\Core\Base\AppShell;
use Mailservice\Balanceserver\Core\Base\Event;
use Mailservice\Balanceserver\Utility\Exception\BuildException;
use Mailservice\Balanceserver\Utility\Exception\Error;

use Mailservice\Balanceserver\Services\ClusterService;

use Mailservice\Balanceserver\Contracts\Cluster\Settings\Server;
use Mailservice\Balanceserver\Contracts\Cluster\Settings\ActiveClusterServers;

use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SubCycle;
use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SubCycleHead;
use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SmtpSettings;

use Mailservice\Balanceserver\Contracts\Cluster\Transports\LetterPacage;



/**
 * Contolls email subpackages and it send them
 * to different active nodes in cluster.
 * 
 */
class ClusterController 
{   
    /**
     * @access private
     * @var static
     * @var ActiveClusterServers
     * 
     * Constructed 
     * @see startNewCycle
     * 
     */
    private static ActiveClusterServers $activeServers; 

    public function __construct()
    {
        self::checkCluster();
    }

    /**
     * @method cycleController
     * @return Fiber
     * 
     * Brakes subcycles & send node packages.
     * 
     */
    public function cycleController( ): \Fiber
    {   
        $startWithCurrentServer =  current(self::$activeServers->serverList);
        return new \Fiber( function( SubCycle $_sub_cycle ) use($startWithCurrentServer): void 
        {   
            $server = $startWithCurrentServer;
            
            foreach( $this->cleateLetterPacage( $_sub_cycle ) as $letterPacage)
            {   
                $node = $this->sendPacageToNode($letterPacage);
                $node->start( $server );
                $server =  next(self::$activeServers->serverList);
                if(!$server)
                {
                    reset(self::$activeServers->serverList);
                    $server = current(self::$activeServers->serverList);
                };

            }
            \Fiber::suspend();
        });
    }

    /**
     * 
     * @method cleateLetterPacage
     * @var SubCycle
     * @return Generator
     * Create letter pacages set in index. Each package is send to 
     * next node in line and do not wait to process reponse. Report is send
     * to report.php
     * 
     */
    private function cleateLetterPacage( SubCycle $_sub_cycle ): \Generator
    {   
        $letterPacage = 1;
        $letters = new LetterPacage( $_sub_cycle );
        
        foreach( $_sub_cycle->emailQueue as $letterToSend )
        {   
            $letters->addToPacage( $letterToSend );

            /**
             * If there is less email than burst parameter it is sended as one 
             * subcycle package.
             */
            $LTPACK = (count($_sub_cycle->emailQueue) < MAIL_BURST_SIZE) ? count($_sub_cycle->emailQueue):MAIL_BURST_SIZE;
            
            if( $letterPacage >= $LTPACK )
            {   
                $letterPacage = 1;
                yield $letters;
                $letters = new LetterPacage( $_sub_cycle );

            }else $letterPacage++;
            
        }
        
        if(count($letters->emailList) > 0) yield $letters;
        
    }

    /**
     * @access private
     * @method sendPacageToNode
     * @var LetterPacage
     * 
     * Send execution with curl post.
     * 
     */
    private function sendPacageToNode(  LetterPacage $_letter_pacage_to_send )
    {   
        return new \Fiber( function( Server $_server ) use ($_letter_pacage_to_send): void 
        {  
            $service = new ClusterService( $_server ); 
            
            AppShell::report( Event::PACKAGE_SEND,
            ' |Node serverId:' . $_server->serverId . ' IP:' .
            $_server->serverIp . ':' . $_server->serverPort
            );
            $service->postToNode( $_letter_pacage_to_send->package() ) ;
            
        });
    }
      
    /**
     * 
     * @access private
     * @method checkCluster
     * Check all registrated node server in cluster.
     * 
     */
    private static function checkCluster()
    {   
        try
        {          
            if(!file_exists( ROOT . CLUSTER_SETTINGS )) 
                throw new \Exception("Missing cluster settings file - clusterSettings.json");

            $activeClusterServers = new ActiveClusterServers();
            
            $checkIfServerIsUp = new \Fiber( function(): void 
            {
                foreach (json_decode( file_get_contents( ROOT . CLUSTER_SETTINGS )) as $key => $serverSettings ) 
                {   
                    $serverId = $serverSettings[0]->serverId;
                    $host = $serverSettings[0]->host;
                    $port = $serverSettings[0]->port;

                    if( $serverId && $host && $port)
                    {
                        \Fiber::suspend(ClusterService::checkConnection( new Server(
                            $serverId,
                            $host,
                            (int)$port
                        )));                    
                    }
                }
            });

            $serverStatus = $checkIfServerIsUp->start();
            if( $serverStatus != false )
                $activeClusterServers->add( $serverStatus );

            while( !$checkIfServerIsUp->isTerminated() ) $checkIfServerIsUp->resume();

            if( empty($activeClusterServers) )
                throw new Exception("Critical error - no node server available");

            self::$activeServers = $activeClusterServers;
            unset($activeClusterServers);
           
        }
        catch( Exception $e )
        {
            throw new BuildException( Error::E2401, $e->getMessage() );
        }
        
    }
}