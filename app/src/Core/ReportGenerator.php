<?php

namespace Mailservice\Balanceserver\Core;

use Mailservice\Balanceserver\Core\Base\Event;
use Mailservice\Balanceserver\Core\Base\IAppShell as ASH;

class ReportGenerator 
{   
    private static string $reportStorageDirectory = ROOT . 'reports/';

    private static string $reportFile;

    private static string $reportExt = '.txt';

    public function __construct( int $_cycle_timestamp )
    {
        self::selectFile( $_cycle_timestamp );
    }
    public function reportLine( Event $_event, string $_event_note = '' )
    {
        self::prapairAndReport( $_event, $_event_note);
    }
    public static function reportFile(): string 
    {
        return self::$reportFile;
    }
    
    public static function selectFile( int $_cycle_timestamp )
    {
        $file =  'report_'.date("Y_m_d", $_cycle_timestamp  );
        self::$reportFile = self::$reportStorageDirectory.$file . '_' . md5($_cycle_timestamp).self::$reportExt; 
    }
    
    public static function prapairAndReport( Event $_event, string $_event_note = '' ): void 
    {           
        switch ( $_event ) 
        {
            case Event::NEW_CYCLE: self::reportNewCycle( $_event_note ); break;

            case Event::SUB_CYCLE: self::writeInReport( $_event->eventDescription() .' '. $_event_note ); break;
            
            default: self::writeInReport( $_event->eventDescription() ." - ". $_event_note . ASH::BR ); break;
        }
    }

    public static function report( int $_cycle_ts,  Event $_event, string $_event_note = '' ): void 
    {   
        self::selectFile( $_cycle_ts );
        
        self::prapairAndReport( $_event, $_event_note );
        
    }

    private static function reportNewCycle( string $_event_note ): void 
    {
        self::writeInReport(
            strtoupper(Event::NEW_CYCLE->eventDescription()). ' ' .
            date("Y m d. (H:i:s)") . " " . $_event_note
            . ASH::BR
        );
    }

    /**
     * @access private
     * @method writeInReport
     * @var event_log_line
     * @return void
     */
    private static function writeInReport( string $_event_log_line ): void
    {   
        file_put_contents( self::$reportFile, $_event_log_line, FILE_APPEND | LOCK_EX );
    }

}