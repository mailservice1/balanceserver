<?php

namespace Mailservice\Balanceserver\Core;

use Mailservice\Balanceserver\Core\Base\AppShell;
use Mailservice\Balanceserver\Database\DataAccess\Databases;
use Mailservice\Balanceserver\Database\DataAccess\NewsletterData;
use Mailservice\Balanceserver\Core\Base\Event;
use Mailservice\Balanceserver\Utility\Exception\RuntimeException;
use Mailservice\Balanceserver\Utility\Tools;


use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SubCycle;
use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SubCycleHead;
use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SmtpSettings;

use Mailservice\Balanceserver\Contracts\NewsletterList;
use Mailservice\Balanceserver\Contracts\CycleUtility;



class Sequence extends AppShell
{   
    private static string $database; 

    public function __construct( string $_selectedDatabase )
    {
        self::$database = $_selectedDatabase;
    }     

    public function run(  ): void
    {   
        try
        {   
            AppShell::report( Event::SUB_CYCLE, self::$database . ' |-' );
            # Check if mail exists?
            $mailsToBeSend = new Databases;
            if( $mailsToBeSend->tablesExists( self::$database ) == false ) throw new RuntimeException( Event::NO_TABLE );

            # Get waiting email queue
            $checkForEmailQueue = new NewsletterData( self::$database );
            $emailQueue = $checkForEmailQueue->fetchList();
            
            if( $emailQueue == null ) throw new RuntimeException( Event::NO_EMAIL_QUEUE );
            
            # Create new subcycle !
            $subCycle = $this->createSubcycle( $emailQueue );

            # Forward subcycle to controller.
            $clusterSequence = self::$clusterController->cycleController();

            # Start subcycle!
            $clusterSequence->start($subCycle);
            while( !$clusterSequence->isTerminated() ) $clusterSequence->resume();
            
        }
        catch( RuntimeException $e )
        {   
            AppShell::report( $e->getEvent(), $e->getNote() );
        }
        
    }

    /**
     * 
     * @access private
     * @method createSubcycle
     * @var NewsletterList
     * @return SubCycle
     * Creates new cycle for each database. 
     * 
     */
    private function createSubcycle( NewsletterList $_waitingEmailsList ): SubCycle
    {   
        $waitingEmails = $_waitingEmailsList->newsletterlist;
        $waitingEmailsCount =  count($waitingEmails);

        $subCycleId = uniqid("Scy_", true );
        # Report waiting emails and close line
        AppShell::report( Event::EMAIL_QUEUE, $waitingEmailsCount . ' |SubCycle ID: ' . $subCycleId );

        # Check for smtp settings
        $smtpData = new Databases( self::$database );
        $smtpSettings = $smtpData->data();
        
        if( $smtpSettings  != false )
        {
            try
            {   
                if( !$smtpSettings->smtp_server_host ) throw new Exception("missing server host");
                if( !Tools::verifySmtpHost( $smtpSettings->smtp_server_host ) ) throw new Exception("incorrect form of smtp server host");
                if( !$smtpSettings->smtp_server_port ) throw new Exception("missing server host");

                // TODO: PASSWORD DECRYPT
                $smtpSettings = new SmtpSettings(
                    (string)$smtpSettings->smtp_server_host,
                    (int)$smtpSettings->smtp_server_port,
                    (string)$smtpSettings->smtp_username,
                    (string)$smtpSettings->smtp_password_hash,
                    (string)$smtpSettings->smtp_email_sender,
                    (string)$smtpSettings->reply_to,
                    (string)$smtpSettings->openssl_iv,
                    (bool)$smtpSettings->smtp_secure
                );
            }
            catch( Exception $switchToEnvironment )
            {   
                AppShell::report( Event::BROKEN_EMAIL, $switchToEnvironment->getMessage() );
                $smtpSettings = parent::$smtpSettings;
            }

        } else $smtpSettings = parent::$smtpSettings;

        $utility = new CycleUtility( self::$database );
        
        
        # Create new subcycle
        return new SubCycle(
            new SubCycleHead( 
                self::$cycleSession->cycle->cycleId,
                self::$cycleSession->cycle->cycleTimestamp,
                $subCycleId,
                $waitingEmailsCount 
            ),
            $smtpSettings,
            array_map( 'Mailservice\Balanceserver\Core\Base\AppShell::emailPacage', $waitingEmails),
            $utility->encrypted($_ENV['UTILITY_KEY'])
        );

        
    }
    

}