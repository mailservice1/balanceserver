<?php

namespace Mailservice\Balanceserver\Services;

use Mailservice\Balanceserver\Core\Base\AppShell;
use Mailservice\Balanceserver\Utility\Curl;
use Mailservice\Balanceserver\Utility\CurlResponse;
use Mailservice\Balanceserver\Utility\CurlException;
use Mailservice\Balanceserver\Contracts\Cluster\Settings\Server;
use Mailservice\Balanceserver\Core\Base\Event;
use Mailservice\Balanceserver\Utility\Exception\Error;


class ClusterService extends Curl
{   
    private static int $excpectedCheckConnectionResponseCode = 200;


    public static function connectTo( Server $_serverSettings  ): CurlResponse | false
    {   
        try
        {
            return parent::connect( $_serverSettings );
        }
        catch( CurlException $e )
        {   
            AppShell::report(Event::SERVER_CONNECTION, $e->getMessage() );
            return false;
        }
        
    }
    public static function checkConnection( Server $_serverSettings ): Server|false
    {   
        $succesfullConnection = self::connectTo($_serverSettings );
        if($succesfullConnection !== false)
        {

            if( $succesfullConnection->responseCode == self::$excpectedCheckConnectionResponseCode ) return $_serverSettings;
                else{
                    AppShell::report(Event::SERVER_UNREACHABLE, ' Responed with: ' . $succesfullConnection->responseCode );
                    return false;
                }

        }else return false;

    }

    public function postToNode( mixed $_content  )
    {   
        $response = $this->post( $_content );
    }
}