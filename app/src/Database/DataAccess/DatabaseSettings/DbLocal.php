<?php

namespace Mailservice\Balanceserver\Database\DataAccess\DatabaseSettings;

use Medoo\Medoo;

abstract class DbLocal
{   
    protected static Medoo $database;
    
    public function __construct( string $_database = "")
    {
        
        try
        {   
            self::$database = new Medoo([

                'type' => $_ENV['DATABASE_TYPE'],
                'host' => $_ENV['DATABASE_HOST'],
                'database' => $_database,
                'username' => $_ENV['DATABASE_USER'],
                'password' => $_ENV['DATABASE_PASSWORD']
    
            ]);

        }catch(\PDOException $e)
        {
            #echo $e->getMessage();
            #exit();
            /**
             * // TODO: How to report database error? Handleing at start !  
             * @see loadEnvironment ->  Aplication
             */
            
        }
    }
}