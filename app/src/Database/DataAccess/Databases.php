<?php

namespace Mailservice\Balanceserver\Database\DataAccess;

use Mailservice\Balanceserver\Database\Entity\Smtpsettings;

class Databases extends DatabaseSettings\DbLocal
{   
    private static string $tableName = 'newsletter_queue';

    public function fetchAll(): array
    {       
        try
        {   
            return parent::$database->query("SHOW DATABASES LIKE 'acm%'")->fetchAll();
        }
        catch(\PDOException $e)
        {
            #echo $e->getMessage();
            // TODO: handle no database error*?? 
        }
    }
    public function data(): Smtpsettings | false
    {
        try
        {   
            if( $data = parent::$database->query("SELECT * FROM page_settings_smtp")->fetchAll() )
            {               
               $settings = new Smtpsettings(
                    (string)$data[0]['page_settings_id'],
                    (string)$data[0]['smtp_server_host'],
                    (string)$data[0]['smtp_server_port'],
                    (string)$data[0]['smtp_username'],
                    (string)$data[0]['smtp_password_hash'],
                    (string)$data[0]['smtp_email_sender'],
                    (int)$data[0]['smtp_secure'],
                    (string)$data[0]['openssl_iv'],
                    (string)$data[0]['reply_to']
               );
               
               return $settings;

            }else return false;
        }
        catch(\PDOException $e)
        {   
            // TODO: REPORT MESSAGE ?
            // TODO: BUGFIX - reporting smtp error - It can be handled at insertion!!! ???
            //echo $e->getMessage();
        }
    }
    public function tablesExists( string $_database ): bool
    {       
        try
        {   
            return parent::$database->query("SHOW TABLE STATUS FROM " . $_database . " WHERE Name = '". self::$tableName  ."' ")->fetchAll() ? true: false;
        }
        catch(\PDOException $e)
        {
            return false;
        }
    }
}
