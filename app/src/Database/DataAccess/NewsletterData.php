<?php

namespace Mailservice\Balanceserver\Database\DataAccess;

use Mailservice\Balanceserver\Database\Entity\NewsletterQueue;
use Mailservice\Balanceserver\Contracts\NewsletterList;
use Mailservice\Balanceserver\Utility\Db\DatabaseEntity;

class NewsletterData extends DatabaseSettings\DbLocal
{   
    /**
     * @access private
     * 
     */

    private static string $dataTable = 'newsletter_queue';

    /**
     * @var NewsletterList
     */
    private NewsletterList $dataRecord;

    public function __construct( string $_database )
    {   
        parent::__construct( $_database );

        $this->dataRecord = new NewsletterList;
    }

    public function populate(): void
    {   
        try
        {
            foreach(
                parent::$database->select( self::$dataTable, [
                    'id',
                    'to_email',
                    'subject',
                    'body',
                    'is_html',
                    'datetime_insert',
                    'send_error',
                    'send_date'
                ]) as $record
                )
                {   
                    (!$record['send_error']) ? (
                        $this->dataRecord->add(new NewsletterQueue(
                            (int)$record['id'],
                            (string)$record['to_email'],
                            (string)$record['subject'],
                            (string)$record['body'],
                            (bool)$record['is_html'],
                            new \DateTime($record['datetime_insert']),
                            (bool)$record['send_error'],
                            ($record['send_date']) ? new \DateTime($record['send_date']): null
                        ))
                    ):"optional report error";
                }
        }
        catch( \PDOException $e )
        {
            // TODO: can be reported in error report - optional
        }
    }
    
    public function fetchList(): NewsletterList | null
    {   
        $this->populate();
        
        if( !empty($this->dataRecord->newsletterlist) ) return $this->dataRecord;
        return null;
    }

    public function find( int $_email_id ): array
    {
        return parent::$database->select(self::$dataTable, [
            'id',
            'to_email',
            'subject',
            'body',
            'is_html',
            'datetime_insert',
            'send_error',
            'send_date'
        ],["id[=]" => $_email_id] );
    }

    public function insert( array $_newsletter )
    {
        return parent::$database->insert(self::$dataTable ,$_newsletter);
    }
    public function update( array $_update )
    {
        return parent::$database->update(self::$dataTable ,$_update);
    }
}

