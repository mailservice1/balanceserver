<?php

namespace Mailservice\Balanceserver\Database\Entity;

readonly class NewsletterQueue
{
    public int $id;

    public string $to_email;

    public string $subject;

    public string $body;

    public bool $is_html;

    public \DateTime $datetime_insert;

    public bool|null $send_error;

    public \DateTime|null $send_date;

    public function __construct(

        int $_id,
        string $_to_email,
        string $_subject,
        string $_body,
        bool $_is_html,
        \DateTime $_datetime_insert,
        bool|null $send_error,
        \DateTime|null $send_date

        ){

        $this->id = $_id;
        $this->to_email = $_to_email;
        $this->subject = $_subject;
        $this->body = $_body;
        $this->is_html = $_is_html;
        $this->datetime_insert = $_datetime_insert;
        $this->send_error = $send_error;
        $this->send_date = $send_date;
    }
  
}