<?php

namespace Mailservice\Balanceserver\Database\Entity;

readonly class Smtpsettings
{
    public int $page_settings_id;

    public string $smtp_server_host;

    public string $smtp_server_port;

    public string $smtp_username;

    public string $smtp_password_hash;

    public string $smtp_email_sender;

    public int $smtp_secure;

    public string $openssl_iv;

    public string $reply_to;

    public function __construct(

        int $_page_settings_id,
        string $_smtp_server_host,
        string $_smtp_server_port,
        string $_smtp_username,
        string $_smtp_password_hash,
        string $_smtp_email_sender,
        int $_smtp_secure,
        string $_openssl_iv,
        string $_reply_to

        ){

        $this->page_settings_id = $_page_settings_id;

        $this->smtp_server_host = $_smtp_server_host;

        $this->smtp_server_port = $_smtp_server_port;

        $this->smtp_username = $_smtp_username;

        $this->smtp_password_hash = $_smtp_password_hash;

        $this->smtp_email_sender = $_smtp_email_sender;

        $this->smtp_secure = $_smtp_secure;

        $this->openssl_iv = $_openssl_iv;

        $this->reply_to = $_reply_to;

    }
  
}