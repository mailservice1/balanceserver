<?php

namespace Mailservice\Balanceserver\Contracts\Cluster;

readonly class Letter 
{
    public int $id;

    public string $toEmail;

    public string $subject;

    public string $body;

    public bool $isHtml;



    public function __construct(

        int $_id,
        string $_to_email,
        string $_subject,
        string $_body,
        string $_is_html

    ){
        $this->id = $_id;
        $this->toEmail = $_to_email;
        $this->subject = $_subject;
        $this->body = $_body;
        $this->isHtml = $_is_html;
    }
}