<?php

namespace Mailservice\Balanceserver\Contracts\Cluster\Transports;

use Mailservice\Balanceserver\Utility\ContractModel\Package;
use Mailservice\Balanceserver\Contracts\Cluster\Letter;

use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SubCycleHead;
use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SmtpSettings;
use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SubCycle;





class LetterPacage implements Package
{   

    public SubCycleHead $head;

    public SmtpSettings $smtpSettings;

    public array $emailList = [];

    public string $utility;

    public function __construct( SubCycle $_sub_cycle )
    {
        $this->head = $_sub_cycle->subCycleHead;
        $this->smtpSettings = $_sub_cycle->smtpSettings;
        $this->utility = $_sub_cycle->utility;
    }
    public function addToPacage( Letter $_letter_to_add )
    {
        array_push( $this->emailList, $_letter_to_add);
    }
    public function package(): string
    {
        return json_encode( $this );
    }
}