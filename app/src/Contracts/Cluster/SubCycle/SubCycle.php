<?php

namespace Mailservice\Balanceserver\Contracts\Cluster\SubCycle;

class SubCycle
{
    public SubCycleHead $subCycleHead;

    public SmtpSettings $smtpSettings;

    public array $emailQueue;

    public string $utility;

    public function __construct(

        SubCycleHead $_subCycleHead,
        SmtpSettings $_smtpSettings,
        array $_email_queue,
        string $_utility

    ){
        $this->subCycleHead = $_subCycleHead;
        $this->smtpSettings = $_smtpSettings;
        $this->emailQueue = $_email_queue;
        $this->utility = $_utility;
    }
}