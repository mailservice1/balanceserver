<?php

namespace Mailservice\Balanceserver\Contracts\Cluster\SubCycle;

readonly class SubCycleHead
{
    public string $cycleId;

    public int $cycleTs;

    public string $subcycleId;

    public int $waitingEmailCount;

    public function __construct(
        
        string $_cycle_id,
        int $_cycle_ts,
        string $_subcycle_id,
        int $_email_count
    )
    {
        $this->cycleId = $_cycle_id;

        $this->cycleTs = $_cycle_ts;

        $this->subcycleId =  $_subcycle_id;

        $this->waitingEmailCount = $_email_count;
        
    }
}