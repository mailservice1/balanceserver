<?php

namespace Mailservice\Balanceserver\Contracts\Cluster\Settings;

use Mailservice\Balanceserver\Contracts\Cluster\Settings\Server;


class ActiveClusterServers
{   
    public array $serverList = [];

    public function add( Server $_add_new_server ): void 
    {
        array_push( $this->serverList, $_add_new_server);
    }
}