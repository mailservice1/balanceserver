<?php

namespace Mailservice\Balanceserver\Contracts\Cluster\Settings;

readonly class Server 
{
    public string $serverId;

    public string $serverIp;

    public int $serverPort;

    public ?string $serverCypher;

    public function __construct(

        string $_serverId,
        string $_serverIp,
        int $_serverPort,
        ?string $_serverCypher = ""
        
    ){
        $this->serverId = $_serverId;
        $this->serverIp = $_serverIp;
        $this->serverPort = $_serverPort;
        $this->serverCypher = $_serverCypher;
    }
}