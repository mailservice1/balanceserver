<?php

namespace Mailservice\Balanceserver\Contracts;

use Mailservice\Balanceserver\Utility\ContractModel\Package;

class CycleSession
{
    public Cycle $cycle;

    public function __construct( Cycle $_cycle )
    {   
        $this->cycle = $_cycle;
    }

}