<?php

namespace Mailservice\Balanceserver\Contracts;

use Mailservice\Balanceserver\Database\Entity\NewsletterQueue;


class NewsletterList 
{   
    /**
     * 
     * @access public
     * @var array newsletterlist
     * 
     */
    public array $newsletterlist = [];

    /**
     * 
     * @method add
     * @var NewsletterQueue
     * @return NewsletterList
     * 
     */
    public function add( NewsletterQueue $_newsLetterQueue ): void
    {
        array_push( $this->newsletterlist, $_newsLetterQueue );
    }
}