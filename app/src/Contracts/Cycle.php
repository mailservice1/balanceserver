<?php

namespace Mailservice\Balanceserver\Contracts;

use Mailservice\Balanceserver\Utility\ContractModel\Describable;
use Mailservice\Balanceserver\Utility\ContractModel\Signed;


class Cycle implements Describable, Signed
{
    public string $cycleId;

    public int $cycleTimestamp;

    public string $cycleHash;

    public function __construct( string $_cycleId, int $_cycle_timestamp )
    {
        $this->cycleId = $_cycleId;

        $this->cycleTimestamp = $_cycle_timestamp;

        $this->hash();

    }
    public function hash(): void 
    {
        $this->cycleHash = md5( $this->cycleId . $this->cycleTimestamp );
    }
    public function describe(): string 
    {
        return "Cycle Id: " . $this->cycleId . " started: " . date("Y-M d. (H:i:s)", $this->cycleTimestamp );
    }
}