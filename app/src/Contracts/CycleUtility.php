<?php

namespace Mailservice\Balanceserver\Contracts;

use Mailservice\Balanceserver\Utility\Encryption;

class CycleUtility extends Encryption 
{
    public string $cycleDatabase;

    public function __construct( string $_cycle_database )
    {
        $this->cycleDatabase = $_cycle_database;
    }

    /**
     * @method decrypt
     * @var utility cypher
     * @var key - environmen key
     * 
     * @return string
     * @return database !
     * 
     */
    public static function decrypt( string $_cypher_text, string $_key ): string
    {
        $data = json_decode( self::sslDecrypt( $_cypher_text, $_key) );
        return $data->cycleDatabase;
    }
   
}