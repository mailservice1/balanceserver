<?php

namespace Mailservice\Balanceserver;

use Mailservice\Balanceserver\Utility\Exception\BuildException;
use Mailservice\Balanceserver\Utility\Exception\Error as ERROR;
use Mailservice\Balanceserver\Database\DataAccess;
use Mailservice\Balanceserver\Contracts\NewsletterList;
use Mailservice\Balanceserver\Database\DataAccess\Databases;
use Mailservice\Balanceserver\Core\Sequence;

final class Application extends Core\Base\AppShell
{   
    /**
     * 
     * @method run
     * @return void
     * 
     * Main applicatin sequence. 
     * 
     */
    public function run(): void
    {
        /**
         * Check source data / database
         */
        parent::loadEnvironment();

        /**
         * Start new Cycle!
         */
        self::$cycleSession = self::startNewCycle();
       
        /**
         * 
         * Create controled fiber for database
         * 
         */
        $databasesequence = new \Fiber( function(): void 
        {
            $checkAllDatabases = new DataAccess\Databases;
            $databases = $checkAllDatabases->fetchAll();
            
            foreach( $databases as $selectedDb)
            {   
                $sequence = new Sequence( $selectedDb[0] );
                $sequence->run() && \Fiber::suspend();
            }

        });

        /**
         * Start application sequence. 
         */
        $databasesequence->start();
        while( !$databasesequence->isTerminated() ) $databasesequence->resume();
        
        echo "Cycle completed! \n Report file: " . self::reportFile() . PHP_EOL;

    }
 
}
