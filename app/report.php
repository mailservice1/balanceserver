<?php declare(strict_types = 1);


define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');


require ROOT . 'vendor/autoload.php';

$loadEnvironment = Dotenv\Dotenv::createImmutable(__DIR__);
$loadEnvironment->safeLoad();

use Mailservice\Balanceserver\Core\Base\AppShell as SH;
use Mailservice\Balanceserver\Database\DataAccess\NewsletterData;

use Mailservice\Balanceserver\Utility\Exception\RuntimeException;
use Mailservice\Balanceserver\Core\Base\Event;

use Mailservice\Balanceserver\Contracts\Cluster\SubCycle\SubCycleHead;
use Mailservice\Balanceserver\Contracts\CycleUtility;

use Mailservice\Balanceserver\Core\ReportGenerator;


/**
 * SCRIPT *** 
 * Report intercepter
 * 
 */
try
{   
    if( $incomeingReport = SH::incomeing() )
    {
        if( !$incomeingReport->subCycleHead )
            throw new RuntimeException( Event::SERVER_RESPONDED, "missing subcycle head" );
        
        new SubCycleHead(
            $incomeingReport->subCycleHead->cycleId,
            $incomeingReport->subCycleHead->cycleTs,
            $incomeingReport->subCycleHead->subcycleId,
            $incomeingReport->subCycleHead->waitingEmailCount
        );

        /**
         * Construct new report generator based od cycle TS
         * Report file is based on cycle ts hash
         */
        $report = new ReportGenerator( $incomeingReport->subCycleHead->cycleTs );

        /**
         * Decode incomeing utility. Key is only presend on Balance server.
         */
        if( !$incomeingReport->utility )
            throw new RuntimeException( Event::RESPONSE_ERROR, "missing subcycle utility cert" );
        $emailQueue = new NewsletterData( CycleUtility::decrypt( $incomeingReport->utility, $_ENV['UTILITY_KEY'] ) );

        # Report response cycle!
        $report->reportLine( Event::RESPONDED, $incomeingReport->subCycleHead->subcycleId);

        $emailInCycle = $incomeingReport->subCycleHead->waitingEmailCount;
        $emailSended = 0;
        $emailError = 0;
        foreach ( $incomeingReport->emailReport as $emailReport ) 
        {   
            # Find retured email by email id
            $letter = $emailQueue->find( $emailReport->emailId );

            # Check sending success ?
            if( $emailReport->timestamp !== false )
            {
                $report->reportLine( Event::RESPONSE_SUCCESS, $letter[0]['to_email'] . date("Y-m-d (H:i:s)", (int)$emailReport->timestamp ));
                $date = new DateTime((string)date('Y/m/d H:i:s',(int)$emailReport->timestamp));

                # Update succesfull send mail to database
                $emailQueue->update([
                    "send_date" => $date->format('Y/m/d H:i:s')
                ],["id[=]" => $emailReport->emailId]);
                $emailSended++;
            }
            else
            {   
                # Update error
                $report->reportLine( Event::RESPONSE_FAILED, $letter[0]['to_email']);
                
                /* //TODO: Uncomment for production
                $emailQueue->update([
                    "send_error" => true
                ],["id[=]" => $emailReport->emailId]);
                */
                $emailError++;
            }        
        }
        # Report response cycle!
        $report->reportLine( Event::RESPONSE_COMPLETED,
        $incomeingReport->subCycleHead->subcycleId . ' | Sended emails: ' .
        $emailSended . "/" . $emailInCycle . ' | NOT SENDED: ' .
        $emailError . "/" . $emailInCycle
        ); 
    }
}
catch( RuntimeException $e)
{
    $report->reportLine( $e->getEvent(), $e->getNote() );
}

