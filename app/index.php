<?php declare(strict_types = 1);

define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/');

/**
 * Define burst pacages
 */
define('MAIL_BURST_SIZE', 10 );

/**
 * Define cluster settings file
 */
define('CLUSTER_SETTINGS','clusterSettings.json' );


require ROOT . 'vendor/autoload.php';

$loadEnvironment = Dotenv\Dotenv::createImmutable(__DIR__);
$loadEnvironment->safeLoad();

use Mailservice\Balanceserver\Utility\Exception\BuildException as Error;

$App = new Mailservice\Balanceserver\Application;

/**
 * Start application sequence
 */
try
{
    $App->run();
}
catch(Error $error)
{
    echo $error->describe();
}
?>